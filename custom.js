$(document).ready(function() {

  $('#booklist').DataTable({
    "language": {
      "info": "Jumlah Data : _TOTAL_ ",
    oPaginate: {
      sNext: '<i class="fas fa-forward"></i>',
      sPrevious: '<i class="fas fa-backward"></i>',
      sFirst: '<i class="fas fa-step-backward"></i>',
      sLast: '<i class="fas fa-step-forward"></i>'
      },
      "infoFiltered": ""
    },
    select: true,
    "paging":   false,
    "bPaginate": true,
    "bLengthChange": false,
    "bFilter": true,
    "bInfo": true,
    "pageLength": 100,
    "bAutoWidth": false,
    "scrollY": 380, //Pengaturan banyak kolom pada datatable
    "scrollX": true,
    responsive: true,
    "autoWidth": false,
    "type"   : "GET",
    "ajax": {
      "url": "https://fullstack-book.ariefdfaltah.com/book/list?key=elan",
      "dataType": "json",
    },
    "columns"    : [
      { 'data': 'id' },
      { 'data': 'updated_at' },
      { 'data': 'created_at' },
      { 'data': 'title' },
      { 'data': 'url_key' },
      { 'data': 'category' },
      { 'data': 'description' },
    ]
  });
  $('#booklist tbody').on( 'click', 'tr', function () {
    var $row = $(this).closest("tr"),        // Finds the closest row <tr> 
    $tds = $row.find("td:nth-child(1)");
    $.each($tds, function() {
      console.log($(this).text());
      var x = $(this).text();
      alert(x);
    });
  });
// http://fullstack-book.ariefdfaltah.com/book/detail/141?key=elan // detail data
  
  // $('.save').click(function(){
  //   const todo = {
  //     title: "book2",
  //     category: "comic",
  //     description: "none"
  //   };
  //   $.ajax({
  //     url: 'https://fullstack-book.ariefdfaltah.com/book/create?key=elan',
  //     method: "POST",
  //     success: function(result) {
  //       console.log(result)
  //     },
  //     error: function(error){
  //       console.log(`Error ${error}`)
  //     }
  //   })
  // })

});

function testResults (form) {
  const todo = {
    title: form.title.value,
    category: form.category.value,
    description: form.description.value,
  };
  fetch('https://fullstack-book.ariefdfaltah.com/book/create?key=elan', {
    method: 'POST',
    body: JSON.stringify(todo),
    headers: {
      'Content-type': 'application/json; charset=UTF-8'
    }
  }).then(function (response) {
    if (response.ok) {
      return response.json();
    }
    return Promise.reject(response);
  }).then(function (data) {
    console.log(data);
  }).catch(function (error) {
    console.warn('Something went wrong.', error);
  });
}



// JSON Object
// const todo = {
//   title: "book2",
//   category: "comic",
//   description: "none"
// };

// fetch('https://fullstack-book.ariefdfaltah.com/book/create?key=elan', {
// 	method: 'POST',
// 	body: JSON.stringify(todo),
// 	headers: {
// 		'Content-type': 'application/json; charset=UTF-8'
// 	}
// }).then(function (response) {
// 	if (response.ok) {
// 		return response.json();
// 	}
// 	return Promise.reject(response);
// }).then(function (data) {
// 	console.log(data);
// }).catch(function (error) {
// 	console.warn('Something went wrong.', error);
// });
